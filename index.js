var uuid = require('uuid')


var Queue = function(values) {
    var self = this
    self.index = -1
    self.values = values || []
    self.completed = false

    self.add = function(e) {
        self.values.push(e)
    }

    self.get = function() {
        self.index += 1
        if(self.index === self.values.length - 1) {
            self.completed = true
        }
        return self.values[self.index]
    }
}

var Worker = function(queue, cb) {
    var self = this
    self.uuid =  uuid()
    self.tasks = -1
    self.start = function() {
        return new Promise(function(resolve, reject) {
            var recursive = function() {
                self.tasks += 1
                if(!queue.completed) {
                    var value = queue.get()
                    
                    cb(value, self.uuid, queue, recursive, reject)
                }
                else {
                    return resolve('worker :: ' + self.uuid + ' :: ' + self.tasks + ' tasks completed')
                }
            }
            recursive()
        })
    }
}

function display(self) {
    console.log('stack id :: ', self.id)
    console.log('tasks number :: ', self.queue.values.length)
    console.log('num workers :: ', self.workers.length)
}

var Stack = function(options) {
    var self = this
    self.id = uuid()
    self.options = options
    self.queue = new Queue()
    self.workers = []
    for(i=0;i<self.options.num_worker;i++) {
        self.workers.push(
            new Worker(self.queue, self.options.cb)
        )
    }
    self._counter = 0
    self.start = function() {
        display(self)
        return new Promise(function(resolve, reject) {
            self.workers.map(function(w) {
                w.start().then(
                    function(response) {
                        self._counter += 1
                        console.log(response)
                        if(self._counter === self.workers.length) {
                            resolve('stack  :: ' + self.id + ' :: complete.')
                        }
                    },
                    function(error) {
                        console.error(error)
                        self._counter += 1
                        if(self._counter === self.workers.length) {
                            reject('stack  :: ' + self.id + ' :: complete.')
                        }
                    }
                )
            })
        })

    }
}
module.exports.Queue = Queue
module.exports.Worker = Worker
module.exports.Stack = Stack