### simple stack permettant de gérer une queue de taches

    - npm install uuid
```
var Stack = require('./ol-worker').Stack


// callback qui sera executée par le stack
function cb(value, id, queue, next, reject) {
    // value: valeur de la queue
    // id: uuid du worker
    // next: continuer a depiler la queue
    console.log('cb called buy worker with uuid ' + id + 'value: ', value)
    setTimeout(function(){
        next()
    }, 5)
    
}

// creation de 4 workers partageant la meme queue
var stack_one = new Stack({num_worker: 16, cb: cb})

// liste d elements pour alimenter la queue
var elements = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16]

// ajout des elements a la queue
elements.map(function(e) {
    stack_one.queue.add(e)
})

// on demarre le stack
stack_one.start().then(
    function(response) {
        console.log(response)
        process.exit()
    },
    function(error) {
        console.error(error)
        process.exit()
    }
)


```

```
stack id ::  edb3a477-7f11-4e9b-9c2b-d82d5060b44b
tasks number ::  16
num workers ::  16
cb called buy worker with uuid 97b81316-53fc-441c-aa1f-aa4cd61a3dadvalue:  1
cb called buy worker with uuid 53d835f1-40c8-430d-943c-9f0b19734790value:  2
cb called buy worker with uuid f851f21d-3ced-4fda-bc91-4749e29a35d0value:  3
cb called buy worker with uuid 3d9f1069-349a-4747-937e-fadbf7f53656value:  4
cb called buy worker with uuid 553c25b8-f9a2-4456-a299-4408beddbc48value:  5
cb called buy worker with uuid e6b47223-979b-46d9-8b16-bccdf7e812c2value:  6
cb called buy worker with uuid d692027d-c2a1-4239-9dcd-87316f01d560value:  7
cb called buy worker with uuid 30cec574-a3c6-41c6-8697-0b7e4ee7ede1value:  8
cb called buy worker with uuid d034a8f4-bdf2-4bce-82b7-66f6a2bd4937value:  9
cb called buy worker with uuid 1e344a9f-77bf-4231-bc10-7dcde2821fc4value:  10
cb called buy worker with uuid 53ffb762-eebf-4f2c-9aeb-99d3eda1b363value:  11
cb called buy worker with uuid 81143423-d483-4f65-be44-85df97222144value:  12
cb called buy worker with uuid e89047ce-8192-4423-9dc9-1f0c7d10b132value:  13
cb called buy worker with uuid 82cfa721-67ac-4f2d-bd83-67d512243e7evalue:  14
cb called buy worker with uuid 8a3631c6-ec8a-4739-ae32-9865b4d16627value:  15
cb called buy worker with uuid 2ad00b3a-ce5b-4195-992c-c3e6a5e68d82value:  16
worker :: 97b81316-53fc-441c-aa1f-aa4cd61a3dad :: 1 tasks completed
worker :: 53d835f1-40c8-430d-943c-9f0b19734790 :: 1 tasks completed
worker :: f851f21d-3ced-4fda-bc91-4749e29a35d0 :: 1 tasks completed
worker :: 3d9f1069-349a-4747-937e-fadbf7f53656 :: 1 tasks completed
worker :: 553c25b8-f9a2-4456-a299-4408beddbc48 :: 1 tasks completed
worker :: e6b47223-979b-46d9-8b16-bccdf7e812c2 :: 1 tasks completed
worker :: d692027d-c2a1-4239-9dcd-87316f01d560 :: 1 tasks completed
worker :: 30cec574-a3c6-41c6-8697-0b7e4ee7ede1 :: 1 tasks completed
worker :: d034a8f4-bdf2-4bce-82b7-66f6a2bd4937 :: 1 tasks completed
worker :: 1e344a9f-77bf-4231-bc10-7dcde2821fc4 :: 1 tasks completed
worker :: 53ffb762-eebf-4f2c-9aeb-99d3eda1b363 :: 1 tasks completed
worker :: 81143423-d483-4f65-be44-85df97222144 :: 1 tasks completed
worker :: e89047ce-8192-4423-9dc9-1f0c7d10b132 :: 1 tasks completed
worker :: 82cfa721-67ac-4f2d-bd83-67d512243e7e :: 1 tasks completed
worker :: 8a3631c6-ec8a-4739-ae32-9865b4d16627 :: 1 tasks completed
worker :: 2ad00b3a-ce5b-4195-992c-c3e6a5e68d82 :: 1 tasks completed
stack  :: edb3a477-7f11-4e9b-9c2b-d82d5060b44b :: complete.


```